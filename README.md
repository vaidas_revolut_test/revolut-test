# Revolut Test

It is a simple REST application offering a few simple banking operations: adding, withdrawing and transfering money between accounts.

## Using

1. Spark framework (for REST API)
2. JUnit 5 for unit-testing
3. SL4J for logging
4. Apache HTTP Client - for testing REST methods
5. Jackson library for JSON serialization

## How to test

There are totally 4 tests implemented:

1. AccountServiceTest
2. MonetaryOperationServiceTest
3. AccountControllerTest
4. MonetaryOperationControllerTest

The first two are created to test service layer while the latter two tests controller methods.
