package com.revolut.test.controller.utils;

/**
 * Utility class containing useful methods for validation operations.
 *
 * @author Vaidas
 */
public class ValidationUtils {

    /**
     * Ensures that the passed object is not null.
     *
     * @param objToValidate object to validate
     * @return true if object is not null, false - otherwise
     */
    public static boolean notNull(Object objToValidate) {
        return objToValidate != null;
    }

    /**
     * Ensures that the passed String is not null or empty
     *
     * @param objToValidate String to validate
     * @return true if String is not null and contains at least one character, false - otherwise
     */
    public static boolean notNullOrEmpty(String objToValidate) {
        return objToValidate != null && !objToValidate.isEmpty();
    }
}
