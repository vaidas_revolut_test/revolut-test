package com.revolut.test.controller.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.test.controller.model.APIResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ResponseTransformer;

import static spark.Spark.halt;

/**
 * Utility class containing useful methods for transforming response.
 * <br />
 * Implements Spark's {@link ResponseTransformer} class.
 *
 * @author Vaidas
 */
public class ResTransformer implements ResponseTransformer {
    private static final Logger log = LoggerFactory.getLogger(ResTransformer.class);
    private ObjectMapper objMapper = new ObjectMapper();

    @Override
    public String render(Object model) {
        APIResponse apiResponse = (APIResponse) model;
        try {
            return objMapper.writeValueAsString(apiResponse);
        } catch (JsonProcessingException e) {
            log.error("An error occurred while trying to marshall response", e);
            halt(500, "Internal server error while handling a response.");
            return "";
        }
    }
}
