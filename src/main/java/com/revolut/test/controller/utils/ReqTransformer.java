package com.revolut.test.controller.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.test.controller.model.APIRequest;
import com.revolut.test.service.exception.InvalidInputException;

import java.io.IOException;

/**
 * Utility class containing useful methods for transforming requests.
 *
 * @author Vaidas
 */
public class ReqTransformer {

    /**
     * Transforms JSON payload to {@link APIRequest} instance.
     *
     * @param payload JSON payload as String
     * @return transformed request or null if it could not be transformed successfully
     */
    public static APIRequest fromPayload(String payload) {
        if (payload == null || payload.isEmpty()) {
            return null;
        }

        ObjectMapper objMapper = new ObjectMapper();
        objMapper.enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS);
        try {
            return objMapper.readValue(payload, APIRequest.class);
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * Parses String parameter and converts it to {@link Long} instance.
     *
     * @param idParam ID parameter to convert
     * @return Converted ID as Long
     * @throws InvalidInputException If parameter could not be converted to Long
     */
    public static Long getId(String idParam) throws InvalidInputException {
        Long id = convertToLong(idParam);
        if (id != null) {
            return id;
        } else {
            throw new InvalidInputException("Parameter 'id' is either missing or contains an invalid value.");
        }
    }

    private static Long convertToLong(String idParam) {
        if (idParam == null || idParam.isEmpty()) {
            return null;
        }
        try {
            return Long.valueOf(idParam);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
