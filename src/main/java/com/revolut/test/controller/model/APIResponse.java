package com.revolut.test.controller.model;

/**
 * Model used for API response mapping
 *
 * @author Vaidas
 */
public class APIResponse {
    private APIResponseResultType result;
    private String errorMessage;
    private Object retObj;

    private APIResponse() {
    }

    /**
     * Constructs successful response model with no additional data to return.
     *
     * @return Successful response model
     */
    public static APIResponse success() {
        return APIResponse.success(null);
    }

    /**
     * Constructs successful response model with ability to pass additional data.
     *
     * @param retObj additional data to return from server
     * @return Successful response model
     */
    public static APIResponse success(Object retObj) {
        APIResponse response = new APIResponse();
        response.setResult(APIResponseResultType.OK);
        response.setRetObj(retObj);

        return response;
    }

    /**
     * Constructs response model with "ERROR" status
     *
     * @param errorMsg message describing error
     * @return Unsuccessful response model
     */
    public static APIResponse error(String errorMsg) {
        APIResponse response = new APIResponse();
        response.setResult(APIResponseResultType.ERROR);
        response.setErrorMessage(errorMsg);

        return response;
    }

    public APIResponseResultType getResult() {
        return result;
    }

    private void setResult(APIResponseResultType result) {
        this.result = result;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Object getRetObj() {
        return retObj;
    }

    private void setRetObj(Object retObj) {
        this.retObj = retObj;
    }

    /**
     * Response types enum. Possible values:
     * <ul>
     *     <li>{@link APIResponseResultType#OK}</li>
     *     <li>{@link APIResponseResultType#ERROR}</li>
     * </ul>
     *
     * @author Vaidas
     */
    public enum APIResponseResultType {
        OK("OK"),
        ERROR("ERR");

        private String value;

        APIResponseResultType(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
}
