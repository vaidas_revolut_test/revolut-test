package com.revolut.test.controller.model;

import java.math.BigDecimal;

/**
 *  Model used for API request mapping
 *
 * @author Vaidas
 */
public class APIRequest {
    private Long id;
    private Long senderId;
    private Long receiverId;
    private String firstName;
    private String lastName;
    private BigDecimal amount;

    public Long getId() {
        return id;
    }

    public Long getSenderId() {
        return senderId;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
