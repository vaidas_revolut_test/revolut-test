package com.revolut.test.controller;

import com.revolut.test.controller.model.APIRequest;
import com.revolut.test.controller.model.APIResponse;
import com.revolut.test.controller.utils.ReqTransformer;
import com.revolut.test.controller.utils.ValidationUtils;
import com.revolut.test.data.model.Account;
import com.revolut.test.service.AccountService;
import com.revolut.test.service.exception.InvalidInputException;
import spark.Request;
import spark.Response;

import java.util.Queue;

/**
 * Controller designed to handle all account related requests.
 *
 * @author Vaidas
 */
public class AccountController {
    private static final AccountService accountService = new AccountService();

    /**
     * Creates an account and returns its details.
     *
     * @param request request object
     * @param response response object
     * @return Details of created account
     * @throws InvalidInputException If input data provided inside the request was not valid
     */
    public static APIResponse createAccount(Request request, Response response) throws InvalidInputException {
        APIRequest apiRequest = ReqTransformer.fromPayload(request.body());

        if (ValidationUtils.notNull(apiRequest) && ValidationUtils.notNullOrEmpty(apiRequest.getFirstName())
                && ValidationUtils.notNullOrEmpty(apiRequest.getLastName())) {
            Account account = accountService.addAccount(apiRequest.getFirstName(), apiRequest.getLastName());
            return APIResponse.success(account);
        } else {
            throw new InvalidInputException(
                    "'firstName' and/or 'lastName' parameters were not filled or are not valid.");
        }
    }

    /**
     * Finds an account by ID and returns its details.
     *
     * @param request request object
     * @param response response object
     * @return Details of the found account or an error message if the operation was not successful
     * @throws InvalidInputException If input data provided inside the request was not valid
     */
    public static APIResponse accountDetails(Request request, Response response) throws InvalidInputException {
        Long id = ReqTransformer.getId(request.params("id"));
        Account account = accountService.findAccount(id);
        if (account != null) {
            return APIResponse.success(account);
        } else {
            return APIResponse.error("Account with specified ID was not found.");
        }
    }

    /**
     * Finds an account by ID and removes it from the system.
     *
     * @param request request object
     * @param response response object
     * @return Success status if operation was completed successfully or an error message - otherwise
     * @throws InvalidInputException If input data provided inside the request was not valid
     */
    public static APIResponse removeAccount(Request request, Response response) throws InvalidInputException {
        Long id = ReqTransformer.getId(request.params("id"));
        boolean wasDeleted = accountService.removeAccount(id);
        if (wasDeleted) {
            return APIResponse.success();
        } else {
            return APIResponse.error("Account with specified ID was not found.");
        }
    }

    /**
     * Returns a full list of all accounts found in the system.
     *
     * @param request request object
     * @param response response object
     * @return Account list
     */
    public static APIResponse accountList(Request request, Response response) {
        Queue<Account> allAccounts = accountService.getAllAccounts();
        return APIResponse.success(allAccounts);
    }
}
