package com.revolut.test.controller;

import com.revolut.test.controller.model.APIResponse;
import com.revolut.test.controller.utils.ResTransformer;
import com.revolut.test.service.exception.BaseAppException;
import spark.Request;
import spark.Response;

/**
 * Controller designed to handle failing requests.
 *
 * @author Vaidas
 */
public class ExceptionController {

    /**
     * Creates a response indicating error status and message.
     *
     * @param exception exception that occurred during operation execution
     * @param request request object
     * @param response response object
     */
    public static void handleBaseServiceException(BaseAppException exception, Request request, Response response) {
        APIResponse apiResponse = APIResponse.error(exception.getMessage());
        ResTransformer resTransformer = new ResTransformer();
        response.body(resTransformer.render(apiResponse));
    }

}
