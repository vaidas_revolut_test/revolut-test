package com.revolut.test.controller;

import com.revolut.test.controller.model.APIRequest;
import com.revolut.test.controller.model.APIResponse;
import com.revolut.test.controller.utils.ReqTransformer;
import com.revolut.test.controller.utils.ValidationUtils;
import com.revolut.test.service.MonetaryOperationService;
import com.revolut.test.service.exception.BaseAppException;
import com.revolut.test.service.exception.InvalidInputException;
import spark.Request;
import spark.Response;

import java.math.BigDecimal;

/**
 * Controller designed to handle all monetary operations related requests.
 *
 * @author Vaidas
 */
public class MonetaryOperationController {
    private static final MonetaryOperationService monetaryOpService = new MonetaryOperationService();

    /**
     * Finds an account by ID and returns its current balance.
     *
     * @param request request object
     * @param response response object
     * @return Account's balance
     * @throws BaseAppException If error occurred during operation execution
     */
    public static APIResponse checkBalance(Request request, Response response) throws BaseAppException {
        Long id = ReqTransformer.getId(request.params("id"));
        BigDecimal balance = monetaryOpService.getAccountBalance(id);
        return APIResponse.success(balance);
    }

    /**
     * Adds money to account's balance.
     *
     * @param request request object
     * @param response response object
     * @return Success status if operation was completed successfully or an error message - otherwise
     * @throws BaseAppException If error occurred during operation execution
     */
    public static APIResponse addMoney(Request request, Response response) throws BaseAppException {
        APIRequest apiRequest = ReqTransformer.fromPayload(request.body());
        if (ValidationUtils.notNull(apiRequest) && ValidationUtils.notNull(apiRequest.getId())
                && ValidationUtils.notNull(apiRequest.getAmount())) {
            monetaryOpService.addMoney(apiRequest.getId(), apiRequest.getAmount());
            return APIResponse.success();
        } else {
            throw new InvalidInputException("'id' and/or 'amount' parameters were not filled or are not valid.");
        }
    }

    /**
     * Withdraws money from account's balance.
     *
     * @param request request object
     * @param response response object
     * @return Success status if operation was completed successfully or an error message - otherwise (e.g.
     * insufficient funds)
     * @throws BaseAppException If error occurred during operation execution
     */
    public static APIResponse withdrawMoney(Request request, Response response) throws BaseAppException {
        APIRequest apiRequest = ReqTransformer.fromPayload(request.body());
        if (ValidationUtils.notNull(apiRequest) && ValidationUtils.notNull(apiRequest.getId())
                && ValidationUtils.notNull(apiRequest.getAmount())) {
            monetaryOpService.withdrawMoney(apiRequest.getId(), apiRequest.getAmount());
            return APIResponse.success();
        } else {
            throw new InvalidInputException("'id' and/or 'amount' parameters were not filled or are not valid.");
        }
    }

    /**
     * Transfers money between two accounts.
     *
     * @param request request object
     * @param response response object
     * @return Success status if operation was completed successfully or an error message - otherwise (e.g.
     * insufficient funds)
     * @throws BaseAppException If error occurred during operation execution
     */
    public static APIResponse transferMoney(Request request, Response response) throws BaseAppException {
        APIRequest apiRequest = ReqTransformer.fromPayload(request.body());
        if (ValidationUtils.notNull(apiRequest) && ValidationUtils.notNull(apiRequest.getSenderId())
                && ValidationUtils.notNull(apiRequest.getReceiverId())
                && ValidationUtils.notNull(apiRequest.getAmount())) {
            monetaryOpService.transferMoney(apiRequest.getSenderId(), apiRequest.getReceiverId(),
                    apiRequest.getAmount());
            return APIResponse.success();
        } else {
            throw new InvalidInputException(
                    "'senderId', 'receiverId' and/or 'amount' parameters were not filled or are not valid.");
        }
    }
}
