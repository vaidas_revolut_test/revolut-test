package com.revolut.test.data.repository;

import com.revolut.test.data.model.Account;

import java.util.Queue;

/**
 * Interface for repository implementations.
 *
 * @author Vaidas
 */
public interface IRepository {

    /**
     * Returns a list of all accounts stored inside a repository.
     *
     * @return Accounts list
     */
    Queue<Account> getAccounts();

    /**
     * Creates a new account and stores it inside a repository.
     *
     * @param firstName account's first name
     * @param lastName account's last name
     * @return Created account
     */
    Account createAccount(String firstName, String lastName);
}
