package com.revolut.test.data.repository;

import com.revolut.test.data.model.Account;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Repository which stores data in memory using thread-safe data structures to ensure capability to support multiple
 * users simultaneously. It's designed to work as a singleton - only one instance at a time should be used across the
 * system.
 *
 * @author Vaidas
 */
public class InMemoryRepository implements IRepository {
    private static final IRepository INSTANCE = new InMemoryRepository();
    private static final AtomicLong ACCOUNT_COUNTER = new AtomicLong();

    private final Queue<Account> accounts;

    private InMemoryRepository() {
        accounts = new ConcurrentLinkedQueue<>();
    }

    /**
     * Returns an existing instance if it is available or creates a new one.
     *
     * @return Repository instance
     */
    public static IRepository getInstance() {
        return INSTANCE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Queue<Account> getAccounts() {
        return accounts;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Account createAccount(String firstName, String lastName) {
        return new Account(ACCOUNT_COUNTER.incrementAndGet(), firstName, lastName);
    }
}
