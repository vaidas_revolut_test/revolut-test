package com.revolut.test.data.model;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Model class for account entity.
 *
 * @author Vaidas
 */
public class Account {
    private Long id;
    private String firstName;
    private String lastName;
    private BigDecimal balance;

    /**
     * Initiates account instance with provided ID, first name and last name data and sets balance to zero.
     *
     * @param id account identification number
     * @param firstName first name
     * @param lastName last name
     */
    public Account(Long id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        balance = BigDecimal.ZERO;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        if (balance == null) {
            throw new IllegalArgumentException("Balance cannot be set to null.");
        }
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(id, account.id) &&
                Objects.equals(firstName, account.firstName) &&
                Objects.equals(lastName, account.lastName) &&
                Objects.equals(balance, account.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, balance);
    }
}
