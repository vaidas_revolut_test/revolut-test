package com.revolut.test.service;

import com.revolut.test.data.model.Account;
import com.revolut.test.service.exception.AccountNotFoundException;
import com.revolut.test.service.exception.BaseAppException;
import com.revolut.test.service.exception.InvalidInputException;
import com.revolut.test.service.exception.NotEnoughFundsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

/**
 * Service class for monetary operations related operations.
 *
 * @author Vaidas
 */
public class MonetaryOperationService {
    private final Logger log = LoggerFactory.getLogger(MonetaryOperationService.class);

    private final AccountService accountService = new AccountService();

    /**
     * Finds an account and returns its balance.
     *
     * @param id account ID
     * @return Account's balance
     * @throws AccountNotFoundException If account with given ID was not found
     */
    public BigDecimal getAccountBalance(long id) throws AccountNotFoundException {
        Account account = accountService.findAccount(id);
        if (account == null) {
            throw new AccountNotFoundException();
        }

        return account.getBalance();
    }

    /**
     * Adds money to selected account.
     *
     * @param accountId account ID
     * @param amount amount to add
     * @throws AccountNotFoundException If account with given ID was not found
     * @throws InvalidInputException If amount is not a valid positive number
     */
    public void addMoney(long accountId, BigDecimal amount) throws AccountNotFoundException, InvalidInputException {
        validateAmountArgument(amount);
        addMoney(getAccount(accountId), amount);
    }

    private void addMoney(Account account, BigDecimal amount) {
        BigDecimal currentBalance = account.getBalance();
        account.setBalance(currentBalance.add(amount));
    }

    /**
     * Withdraws money from selected account.
     *
     * @param accountId account ID
     * @param amount amount to withdraw
     * @throws NotEnoughFundsException If there is not enough funds in account to complete the operation
     * @throws AccountNotFoundException If account with given ID was not found
     * @throws InvalidInputException If amount is not a valid positive number
     */
    public void withdrawMoney(long accountId, BigDecimal amount) throws NotEnoughFundsException,
            AccountNotFoundException, InvalidInputException {
        validateAmountArgument(amount);
        withdrawMoney(getAccount(accountId), amount);
    }

    private void withdrawMoney(Account account, BigDecimal amount) throws NotEnoughFundsException {
        BigDecimal currentBalance = account.getBalance();
        if (currentBalance.subtract(amount).compareTo(BigDecimal.ZERO) < 0) {
            throw new NotEnoughFundsException();
        }

        account.setBalance(currentBalance.subtract(amount));
    }

    /**
     * Transfers money between two accounts.
     *
     * @param senderAccId sender's account ID
     * @param receiverAccId receiver's account ID
     * @param amount amount to transfer
     * @throws BaseAppException If an error occurred while performing the operation
     */
    public void transferMoney(long senderAccId, long receiverAccId, BigDecimal amount) throws BaseAppException {
        validateAmountArgument(amount);

        Account sender = getAccount(senderAccId);
        Account receiver = getAccount(receiverAccId);
        BigDecimal backupSenderBalance = sender.getBalance();
        BigDecimal backupReceiverBalance = receiver.getBalance();

        try {
            withdrawMoney(sender, amount);
            addMoney(receiver, amount);
        } catch (Exception e) {
            if (e instanceof NotEnoughFundsException) {
                throw e;
            }

            sender.setBalance(backupSenderBalance);
            receiver.setBalance(backupReceiverBalance);
            String errorMsg = String.format(
                    "Error occurred while trying to transfer money from sender '%d' to receiver '%d'.",
                    sender.getId(),
                    receiver.getId());
            log.error(errorMsg, e);
            throw new BaseAppException();
        }
    }

    private void validateAmountArgument(BigDecimal amount) throws InvalidInputException {
        if (amount == null || amount.signum() <= 0) {
            throw new InvalidInputException("Amount must be greater than zero.");
        }
    }

    private Account getAccount(long accountId) throws AccountNotFoundException {
        Account account = accountService.findAccount(accountId);
        if (account == null) {
            throw new AccountNotFoundException();
        }

        return account;
    }
}
