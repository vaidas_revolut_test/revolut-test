package com.revolut.test.service;

import com.revolut.test.data.model.Account;
import com.revolut.test.data.repository.IRepository;
import com.revolut.test.data.repository.InMemoryRepository;

import java.util.Queue;

/**
 * Service class for account related operations.
 *
 * @author Vaidas
 */
public class AccountService {
    private final IRepository repository = InMemoryRepository.getInstance();

    /**
     * Returns queue of all accounts found in a repository.
     *
     * @return Account queue
     */
    public Queue<Account> getAllAccounts() {
        return repository.getAccounts();
    }

    /**
     * Finds an account by ID and returns it.
     *
     * @param id account ID
     * @return Found account or null if it could not be found.
     */
    public Account findAccount(long id) {
        return repository.getAccounts().stream()
                .filter(account -> account.getId() != null && account.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    /**
     * Creates a new account and adds it to the repository.
     *
     * @param firstName account's first name
     * @param lastName account's last name
     * @return Created account or null if operation was not completed successfully
     */
    public Account addAccount(String firstName, String lastName) {
        if (firstName == null || firstName.isEmpty() || lastName == null || lastName.isEmpty()) {
            throw new IllegalArgumentException("firstName and lastName arguments cannot be null or empty.");
        }

        Queue<Account> accounts = repository.getAccounts();
        Account account = repository.createAccount(firstName, lastName);
        return accounts.offer(account) ? account : null;
    }

    /**
     * Finds an account by ID and removes it from the repository.
     *
     * @param accountId account ID
     * @return true if account was removed successfully or false - otherwise
     */
    public boolean removeAccount(long accountId) {
        Queue<Account> accounts = repository.getAccounts();
        return accounts.removeIf(account -> account.getId() != null && account.getId().equals(accountId));
    }
}
