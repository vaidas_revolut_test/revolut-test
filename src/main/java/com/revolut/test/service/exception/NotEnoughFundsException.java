package com.revolut.test.service.exception;

/**
 * Custom exception used to indicate that the operation could not be completed because of lack of funds.
 *
 * @author Vaidas
 */
public class NotEnoughFundsException extends BaseAppException {

    /**
     * Creates an instance with a hard-coded error message.
     */
    public NotEnoughFundsException() {
        super("There are not enough funds in the account therefore an operation was terminated.");
    }

}
