package com.revolut.test.service.exception;

/**
 * All-purpose basic application exception class.
 *
 * @author Vaidas
 */
public class BaseAppException extends Exception {

    /**
     * Creates an instance with a hard-coded error message.
     */
    public BaseAppException() {
        super("An internal application exception occurred. Please contact system administrator.");
    }

    /**
     * Creates an instance with a given error message.
     */
    BaseAppException(String errorMsg) {
        super(errorMsg);
    }
}
