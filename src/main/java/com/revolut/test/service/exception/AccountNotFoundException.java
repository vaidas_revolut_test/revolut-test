package com.revolut.test.service.exception;

/**
 * Custom exception used to indicate that account could not be found.
 *
 * @author Vaidas
 */
public class AccountNotFoundException extends BaseAppException {

    /**
     * Creates an instance with a hard-coded error message.
     */
    public AccountNotFoundException() {
        super("Account could not be found therefore an operation was terminated.");
    }

}
