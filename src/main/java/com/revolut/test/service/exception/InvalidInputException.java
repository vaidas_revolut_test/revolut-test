package com.revolut.test.service.exception;

/**
 * Custom exception used to indicate that the request input is not valid.
 *
 * @author Vaidas
 */
public class InvalidInputException extends BaseAppException {

    /**
     * Creates an instance with a hard-coded error message.
     */
    public InvalidInputException(String errorMsg) {
        super(errorMsg);
    }

}
