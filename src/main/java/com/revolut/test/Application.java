package com.revolut.test;

import com.revolut.test.controller.AccountController;
import com.revolut.test.controller.ExceptionController;
import com.revolut.test.controller.MonetaryOperationController;
import com.revolut.test.controller.utils.ResTransformer;
import com.revolut.test.service.exception.BaseAppException;

import static spark.Spark.*;

/**
 * Main application class with an entry method.
 *
 * @author Vaidas
 */
public class Application {
    private static final String ACCEPT_TYPE_JSON = "application/json";

    /**
     * Application entry method - defines all available REST service's methods.
     *
     * <br />
     * Account related requests:
     * <ul>
     *     <li><i>/account/create</i> - creates an account, saves and returns it</li>
     *     <li><i>/account/details/{accountID}</i> - returns details of an account</li>
     *     <li><i>/account/remove/{accountID}</i> - removes an account</li>
     *     <li><i>/account/list</i> - returns a list of all accounts</li>
     * </ul>
     *
     * Monetary operations related requests:
     * <ul>
     *     <li><i>/monet-op/balance/{accountID}</i> - returns balance of an account</li>
     *     <li><i>/monet-op/add</i> - adds money to an account</li>
     *     <li><i>/monet-op/withdraw</i> - withdraws money from an account</li>
     *     <li><i>/monet-op/transfer</i> - transfers money between two accounts</li>
     * </ul>
     *
     * @param args application start arguments - not required
     */
    public static void main(String[] args) {
        path("/account", ()-> {
            put("/create", ACCEPT_TYPE_JSON, AccountController::createAccount, new ResTransformer());
            get("/details/:id", ACCEPT_TYPE_JSON, AccountController::accountDetails, new ResTransformer());
            delete("/remove/:id", ACCEPT_TYPE_JSON, AccountController::removeAccount, new ResTransformer());
            get("/list", ACCEPT_TYPE_JSON, AccountController::accountList, new ResTransformer());
        });
        path("/monet-op", ()-> {
            get("/balance/:id", ACCEPT_TYPE_JSON, MonetaryOperationController::checkBalance, new ResTransformer());
            post("/add", ACCEPT_TYPE_JSON, MonetaryOperationController::addMoney, new ResTransformer());
            post("/withdraw", ACCEPT_TYPE_JSON, MonetaryOperationController::withdrawMoney, new ResTransformer());
            post("/transfer", ACCEPT_TYPE_JSON, MonetaryOperationController::transferMoney, new ResTransformer());
        });

        exception(BaseAppException.class, ExceptionController::handleBaseServiceException);
    }
}
