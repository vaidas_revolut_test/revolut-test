package com.revolut.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.test.controller.model.APIResponse;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.LinkedHashMap;

public class TestUtils {
    private static final String HOST = "http://localhost:4567";
    private static final String MIME_TYPE_JSON = "application/json";
    public static final String JSON_REQ_ONE_ID = "{\"id\": \"%d\", \"amount\": \"%s\"}";
    public static final String JSON_REQ_TWO_IDS = "{\"senderId\": \"%d\", \"receiverId\": \"%d\", \"amount\": \"%s\"}";

    public static <T extends HttpRequestBase> T createRequest(Class<T> clazz, String url)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<T> constructor = clazz.getConstructor(String.class);
        T request = constructor.newInstance(HOST + url);
        request.setHeader("Accept", MIME_TYPE_JSON);
        request.setHeader("Content-type", MIME_TYPE_JSON);

        return request;
    }

    public static APIResponse retrieveFromResponse(HttpResponse response) throws IOException {
        HttpEntity respEntity = response.getEntity();
        String jsonFromResponse = EntityUtils.toString(respEntity);
        EntityUtils.consume(respEntity);
        return new ObjectMapper().readValue(jsonFromResponse, APIResponse.class);
    }

    public static LinkedHashMap createAccount(CloseableHttpClient client) throws ReflectiveOperationException,
            IOException {
        HttpPut request = TestUtils.createRequest(HttpPut.class, "/account/create");
        String json = "{\"firstName\": \"Firstname\", \"lastName\": \"Lastname\"}";
        request.setEntity(new StringEntity(json));

        LinkedHashMap accountInfo;
        try (CloseableHttpResponse response = client.execute(request)) {
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            accountInfo = (LinkedHashMap) apiResponse.getRetObj();
        }
        return accountInfo;
    }

    public static LinkedHashMap findAccount(CloseableHttpClient client, long accountId)
            throws ReflectiveOperationException, IOException {
        HttpGet request = TestUtils.createRequest(HttpGet.class, "/account/details/" + accountId);
        LinkedHashMap foundAccount;
        try (CloseableHttpResponse response = client.execute(request)) {
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            foundAccount = (LinkedHashMap) apiResponse.getRetObj();
        }
        return foundAccount;
    }

    public static void addMoney(CloseableHttpClient client, long accountId, BigDecimal amount)
            throws ReflectiveOperationException, IOException {
        HttpPost request = TestUtils.createRequest(HttpPost.class, "/monet-op/add");
        String json = String.format(JSON_REQ_ONE_ID, accountId, amount);
        request.setEntity(new StringEntity(json));
        client.execute(request).close();
    }
}
