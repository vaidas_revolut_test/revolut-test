package com.revolut.test.controller;

import com.revolut.test.TestUtils;
import com.revolut.test.controller.model.APIResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.LinkedHashMap;

import static org.junit.jupiter.api.Assertions.*;

class MonetaryOperationControllerTest extends BaseControllerTest {
    private static final BigDecimal HUNDRED = BigDecimal.valueOf(100);
    private static final BigDecimal NINETY = BigDecimal.valueOf(90);
    private static final BigDecimal MINUS_ONE = BigDecimal.valueOf(-1);

    /* checkBalance */

    @Test
    void checkBalance_invalidId() throws IOException, ReflectiveOperationException {
        HttpGet request = TestUtils.createRequest(HttpGet.class, "/monet-op/balance/a");
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.ERROR, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage != null && !errorMessage.isEmpty());
            assertEquals("Parameter 'id' is either missing or contains an invalid value.", errorMessage);
            assertNull(apiResponse.getRetObj());
        }
    }

    @Test
    void checkBalance_notExistingAccount() throws IOException, ReflectiveOperationException {
        Integer id = (Integer) TestUtils.createAccount(client).get("id") + 1;
        HttpGet request = TestUtils.createRequest(HttpGet.class, "/monet-op/balance/" + id);
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.ERROR, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage != null && !errorMessage.isEmpty());
            assertEquals("Account could not be found therefore an operation was terminated.", errorMessage);
            assertNull(apiResponse.getRetObj());
        }
    }

    @Test
    void checkBalance_success() throws IOException, ReflectiveOperationException {
        LinkedHashMap createdAccount = TestUtils.createAccount(client);
        HttpGet request = TestUtils.createRequest(HttpGet.class, "/monet-op/balance/" + createdAccount.get("id"));
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.OK, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage == null || errorMessage.isEmpty());
            assertNotNull(apiResponse.getRetObj());
            assertEquals(createdAccount.get("balance"), apiResponse.getRetObj());
        }
    }

    /* addMoney */

    @Test
    void addMoney_notExistingAccount() throws IOException, ReflectiveOperationException {
        HttpPost request = TestUtils.createRequest(HttpPost.class, "/monet-op/add");
        String json = String.format(TestUtils.JSON_REQ_ONE_ID, -1, BigDecimal.TEN);
        request.setEntity(new StringEntity(json));
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.ERROR, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage != null && !errorMessage.isEmpty());
            assertEquals("Account could not be found therefore an operation was terminated.", errorMessage);
            assertNull(apiResponse.getRetObj());
        }
    }

    @Test
    void addMoney_invalidAmount() throws IOException, ReflectiveOperationException {
        Integer id = (Integer) TestUtils.createAccount(client).get("id");
        HttpPost request = TestUtils.createRequest(HttpPost.class, "/monet-op/add");
        String json = String.format(TestUtils.JSON_REQ_ONE_ID, id, MINUS_ONE);
        request.setEntity(new StringEntity(json));
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.ERROR, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage != null && !errorMessage.isEmpty());
            assertEquals("Amount must be greater than zero.", errorMessage);
            assertNull(apiResponse.getRetObj());
        }
    }

    @Test
    void addMoney_success() throws IOException, ReflectiveOperationException {
        Integer id = (Integer) TestUtils.createAccount(client).get("id");
        HttpPost request = TestUtils.createRequest(HttpPost.class, "/monet-op/add");
        String json = String.format(TestUtils.JSON_REQ_ONE_ID, id, BigDecimal.TEN);
        request.setEntity(new StringEntity(json));
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.OK, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage == null || errorMessage.isEmpty());
            assertNull(apiResponse.getRetObj());
        }

        LinkedHashMap accAfterOperation = TestUtils.findAccount(client, id);
        assertEquals(BigDecimal.TEN, BigDecimal.valueOf((Integer) accAfterOperation.get("balance")));
    }

    /* withdrawMoney */

    @Test
    void withdrawMoney_notExistingAccount() throws IOException, ReflectiveOperationException {
        HttpPost request = TestUtils.createRequest(HttpPost.class, "/monet-op/withdraw");
        String json = String.format(TestUtils.JSON_REQ_ONE_ID, Integer.MAX_VALUE, BigDecimal.TEN);
        request.setEntity(new StringEntity(json));
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.ERROR, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage != null && !errorMessage.isEmpty());
            assertEquals("Account could not be found therefore an operation was terminated.", errorMessage);
            assertNull(apiResponse.getRetObj());
        }
    }

    @Test
    void withdrawMoney_invalidAmount() throws IOException, ReflectiveOperationException {
        Integer id = (Integer) TestUtils.createAccount(client).get("id");
        TestUtils.addMoney(client, id, HUNDRED);
        HttpPost request = TestUtils.createRequest(HttpPost.class, "/monet-op/withdraw");
        String json = String.format(TestUtils.JSON_REQ_ONE_ID, id, BigDecimal.ZERO);
        request.setEntity(new StringEntity(json));
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.ERROR, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage != null && !errorMessage.isEmpty());
            assertEquals("Amount must be greater than zero.", errorMessage);
            assertNull(apiResponse.getRetObj());
        }

        LinkedHashMap accAfterOperation = TestUtils.findAccount(client, id);
        assertEquals(HUNDRED, BigDecimal.valueOf((Integer) accAfterOperation.get("balance")));
    }

    @Test
    void withdrawMoney_notEnoughFunds() throws IOException, ReflectiveOperationException {
        Integer id = (Integer) TestUtils.createAccount(client).get("id");
        TestUtils.addMoney(client, id, BigDecimal.TEN);
        HttpPost request = TestUtils.createRequest(HttpPost.class, "/monet-op/withdraw");
        String json = String.format(TestUtils.JSON_REQ_ONE_ID, id, HUNDRED);
        request.setEntity(new StringEntity(json));
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.ERROR, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage != null && !errorMessage.isEmpty());
            assertEquals("There are not enough funds in the account therefore an operation was terminated.",
                    errorMessage);
            assertNull(apiResponse.getRetObj());
        }

        LinkedHashMap accAfterOperation = TestUtils.findAccount(client, id);
        assertEquals(BigDecimal.TEN, BigDecimal.valueOf((Integer) accAfterOperation.get("balance")));
    }

    @Test
    void withdrawMoney_success() throws IOException, ReflectiveOperationException {
        Integer id = (Integer) TestUtils.createAccount(client).get("id");
        TestUtils.addMoney(client, id, HUNDRED);
        HttpPost request = TestUtils.createRequest(HttpPost.class, "/monet-op/withdraw");
        String json = String.format(TestUtils.JSON_REQ_ONE_ID, id, BigDecimal.TEN);
        request.setEntity(new StringEntity(json));
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.OK, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage == null || errorMessage.isEmpty());
            assertNull(apiResponse.getRetObj());
        }

        LinkedHashMap accAfterOperation = TestUtils.findAccount(client, id);
        assertEquals(NINETY, BigDecimal.valueOf((Integer) accAfterOperation.get("balance")));
    }

    /* transferMoney */

    @Test
    void transferMoney_notExistingSender() throws IOException, ReflectiveOperationException {
        Integer receiverId = (Integer) TestUtils.createAccount(client).get("id");
        TestUtils.addMoney(client, receiverId, HUNDRED);
        Integer senderId = receiverId + 1;
        HttpPost request = TestUtils.createRequest(HttpPost.class, "/monet-op/transfer");
        String json = String.format(TestUtils.JSON_REQ_TWO_IDS, senderId, receiverId, BigDecimal.TEN);
        request.setEntity(new StringEntity(json));
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.ERROR, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage != null && !errorMessage.isEmpty());
            assertEquals("Account could not be found therefore an operation was terminated.", errorMessage);
            assertNull(apiResponse.getRetObj());
        }

        LinkedHashMap receiver = TestUtils.findAccount(client, receiverId);
        assertEquals(HUNDRED, BigDecimal.valueOf((Integer) receiver.get("balance")));
    }

    @Test
    void transferMoney_notExistingReceiver() throws IOException, ReflectiveOperationException {
        Integer senderId = (Integer) TestUtils.createAccount(client).get("id");
        TestUtils.addMoney(client, senderId, NINETY);
        Integer receiverId = senderId + 1;
        HttpPost request = TestUtils.createRequest(HttpPost.class, "/monet-op/transfer");
        String json = String.format(TestUtils.JSON_REQ_TWO_IDS, senderId, receiverId, BigDecimal.ONE);
        request.setEntity(new StringEntity(json));
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.ERROR, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage != null && !errorMessage.isEmpty());
            assertEquals("Account could not be found therefore an operation was terminated.", errorMessage);
            assertNull(apiResponse.getRetObj());
        }

        LinkedHashMap sender = TestUtils.findAccount(client, senderId);
        assertEquals(NINETY, BigDecimal.valueOf((Integer) sender.get("balance")));
    }

    @Test
    void transferMoney_invalidAmount() throws IOException, ReflectiveOperationException {
        Integer senderId = (Integer) TestUtils.createAccount(client).get("id");
        Integer receiverId = (Integer) TestUtils.createAccount(client).get("id");
        TestUtils.addMoney(client, senderId, HUNDRED);
        HttpPost request = TestUtils.createRequest(HttpPost.class, "/monet-op/transfer");
        String json = String.format(TestUtils.JSON_REQ_TWO_IDS, senderId, receiverId, MINUS_ONE);
        request.setEntity(new StringEntity(json));
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.ERROR, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage != null && !errorMessage.isEmpty());
            assertEquals("Amount must be greater than zero.", errorMessage);
            assertNull(apiResponse.getRetObj());
        }

        LinkedHashMap sender = TestUtils.findAccount(client, senderId);
        assertEquals(HUNDRED, BigDecimal.valueOf((Integer) sender.get("balance")));
        LinkedHashMap receiver = TestUtils.findAccount(client, receiverId);
        assertEquals(BigDecimal.ZERO, BigDecimal.valueOf((Integer) receiver.get("balance")));
    }

    @Test
    void transferMoney_notEnoughFunds() throws IOException, ReflectiveOperationException {
        Integer senderId = (Integer) TestUtils.createAccount(client).get("id");
        Integer receiverId = (Integer) TestUtils.createAccount(client).get("id");
        TestUtils.addMoney(client, senderId, NINETY);
        HttpPost request = TestUtils.createRequest(HttpPost.class, "/monet-op/transfer");
        String json = String.format(TestUtils.JSON_REQ_TWO_IDS, senderId, receiverId, HUNDRED);
        request.setEntity(new StringEntity(json));
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.ERROR, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage != null && !errorMessage.isEmpty());
            assertEquals("There are not enough funds in the account therefore an operation was terminated.",
                    errorMessage);
            assertNull(apiResponse.getRetObj());
        }

        LinkedHashMap sender = TestUtils.findAccount(client, senderId);
        assertEquals(NINETY, BigDecimal.valueOf((Integer) sender.get("balance")));
        LinkedHashMap receiver = TestUtils.findAccount(client, receiverId);
        assertEquals(BigDecimal.ZERO, BigDecimal.valueOf((Integer) receiver.get("balance")));
    }

    @Test
    void transferMoney_success() throws IOException, ReflectiveOperationException {
        Integer senderId = (Integer) TestUtils.createAccount(client).get("id");
        Integer receiverId = (Integer) TestUtils.createAccount(client).get("id");
        TestUtils.addMoney(client, senderId, HUNDRED);
        TestUtils.addMoney(client, receiverId, NINETY);
        HttpPost request = TestUtils.createRequest(HttpPost.class, "/monet-op/transfer");
        String json = String.format(TestUtils.JSON_REQ_TWO_IDS, senderId, receiverId, BigDecimal.TEN);
        request.setEntity(new StringEntity(json));
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.OK, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage == null || errorMessage.isEmpty());
            assertNull(apiResponse.getRetObj());
        }

        LinkedHashMap sender = TestUtils.findAccount(client, senderId);
        assertEquals(NINETY, BigDecimal.valueOf((Integer) sender.get("balance")));
        LinkedHashMap receiver = TestUtils.findAccount(client, receiverId);
        assertEquals(HUNDRED, BigDecimal.valueOf((Integer) receiver.get("balance")));
    }

}
