package com.revolut.test.controller;

import com.revolut.test.TestUtils;
import com.revolut.test.controller.model.APIResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AccountControllerTest extends BaseControllerTest {

    @Test
    void notExistingURL() throws ReflectiveOperationException, IOException {
        HttpGet request = TestUtils.createRequest(HttpGet.class, "/notExisting");
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(404, response.getStatusLine().getStatusCode());
        }
    }

    /* createAccount */

    @Test
    void createAccount_emptyInput() throws IOException, ReflectiveOperationException {
        HttpPut request = TestUtils.createRequest(HttpPut.class, "/account/create");
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.ERROR, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage != null && !errorMessage.isEmpty());
            assertEquals("'firstName' and/or 'lastName' parameters were not filled or are not valid.", errorMessage);
            assertNull(apiResponse.getRetObj());
        }
    }

    @Test
    void createAccount_successful() throws IOException, ReflectiveOperationException {
        HttpPut request = TestUtils.createRequest(HttpPut.class, "/account/create");
        String json = "{\"firstName\": \"Firstname\", \"lastName\": \"Lastname\"}";
        request.setEntity(new StringEntity(json));

        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.OK, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage == null || errorMessage.isEmpty());
            LinkedHashMap accountInfo = (LinkedHashMap) apiResponse.getRetObj();
            assertNotNull(accountInfo);
            assertEquals("Firstname", accountInfo.get("firstName"));
            assertEquals("Lastname", accountInfo.get("lastName"));
            assertEquals(0, BigDecimal.ZERO.compareTo(BigDecimal.valueOf((int) accountInfo.get("balance"))));
        }
    }

    /* accountDetails */

    @Test
    void accountDetails_noId() throws IOException, ReflectiveOperationException {
        HttpGet request = TestUtils.createRequest(HttpGet.class, "/account/details");
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(404, response.getStatusLine().getStatusCode());
        }
    }

    @Test
    void accountDetails_invalidId() throws IOException, ReflectiveOperationException {
        HttpGet request = TestUtils.createRequest(HttpGet.class, "/account/details/abc");
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.ERROR, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage != null && !errorMessage.isEmpty());
            assertEquals("Parameter 'id' is either missing or contains an invalid value.", errorMessage);
            assertNull(apiResponse.getRetObj());
        }
    }

    @Test
    void accountDetails_successful() throws IOException, ReflectiveOperationException {
        LinkedHashMap createdAccount = TestUtils.createAccount(client);
        HttpGet request = TestUtils.createRequest(HttpGet.class, "/account/details/" + createdAccount.get("id"));
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.OK, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage == null || errorMessage.isEmpty());
            LinkedHashMap foundAccount = (LinkedHashMap) apiResponse.getRetObj();
            assertNotNull(foundAccount);
            assertEquals(createdAccount.get("id"), foundAccount.get("id"));
            assertEquals(createdAccount.get("firstName"), foundAccount.get("firstName"));
            assertEquals(createdAccount.get("lastName"), foundAccount.get("lastName"));
            assertEquals(createdAccount.get("amount"), foundAccount.get("amount"));
        }
    }

    /* removeAccount */

    @Test
    void removeAccount_invalidId() throws IOException, ReflectiveOperationException {
        HttpDelete request = TestUtils.createRequest(HttpDelete.class, "/account/remove/aaa111");
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.ERROR, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage != null && !errorMessage.isEmpty());
            assertEquals("Parameter 'id' is either missing or contains an invalid value.", errorMessage);
            assertNull(apiResponse.getRetObj());
        }
    }

    @Test
    void removeAccount_notExistingAccount() throws IOException, ReflectiveOperationException {
        LinkedHashMap createdAccount = TestUtils.createAccount(client);
        Integer id = (Integer) createdAccount.get("id") + 1;
        HttpDelete request = TestUtils.createRequest(HttpDelete.class, "/account/remove/" + id);
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.ERROR, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage != null && !errorMessage.isEmpty());
            assertEquals("Account with specified ID was not found.", errorMessage);
            assertNull(apiResponse.getRetObj());
        }
    }

    @Test
    void removeAccount_existingAccount() throws IOException, ReflectiveOperationException {
        LinkedHashMap createdAccount = TestUtils.createAccount(client);
        HttpDelete request = TestUtils.createRequest(HttpDelete.class, "/account/remove/" + createdAccount.get("id"));
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.OK, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage == null || errorMessage.isEmpty());
            assertNull(apiResponse.getRetObj());
        }
    }

    /* accountList */

    @Test
    @SuppressWarnings("unchecked")
    void accountList_success()throws IOException, ReflectiveOperationException {
        TestUtils.createAccount(client);
        HttpGet request = TestUtils.createRequest(HttpGet.class, "/account/list");
        try (CloseableHttpResponse response = client.execute(request)) {
            assertEquals(200, response.getStatusLine().getStatusCode());
            APIResponse apiResponse = TestUtils.retrieveFromResponse(response);
            assertEquals(APIResponse.APIResponseResultType.OK, apiResponse.getResult());
            String errorMessage = apiResponse.getErrorMessage();
            assertTrue(errorMessage == null || errorMessage.isEmpty());
            assertNotNull(apiResponse.getRetObj());
            List<LinkedHashMap> accountList = (List<LinkedHashMap>) apiResponse.getRetObj();
            assertTrue(accountList != null && !accountList.isEmpty());
            accountList.forEach((account) -> {
                assertNotNull(account.get("id"));
                String firstName = (String) account.get("firstName");
                assertTrue(firstName != null && !firstName.isEmpty());
                String lastName = (String) account.get("firstName");
                assertTrue(lastName != null && !lastName.isEmpty());
                assertEquals(BigDecimal.ZERO, BigDecimal.valueOf((Integer) account.get("balance")));
            });
        }
    }

}
