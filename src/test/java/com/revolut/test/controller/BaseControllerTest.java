package com.revolut.test.controller;

import com.revolut.test.Application;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import spark.Spark;

import java.io.IOException;

class BaseControllerTest {
    static final CloseableHttpClient client = HttpClients.createDefault();

    @BeforeAll
    static void beforeTest() {
        Application.main(null);
    }

    @AfterAll
    static void afterTest() throws IOException {
        Spark.stop();
        client.close();
    }
}
