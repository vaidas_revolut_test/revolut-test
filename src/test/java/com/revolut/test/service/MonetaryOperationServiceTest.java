package com.revolut.test.service;

import com.revolut.test.data.model.Account;
import com.revolut.test.service.exception.AccountNotFoundException;
import com.revolut.test.service.exception.BaseAppException;
import com.revolut.test.service.exception.InvalidInputException;
import com.revolut.test.service.exception.NotEnoughFundsException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Queue;

import static org.junit.jupiter.api.Assertions.*;

class MonetaryOperationServiceTest {
    private AccountService accountService = new AccountService();
    private MonetaryOperationService monetaryOpService = new MonetaryOperationService();

    private static final BigDecimal POS_AMOUNT = BigDecimal.valueOf(123.45);
    private static final BigDecimal NEG_AMOUNT = BigDecimal.valueOf(-123.45);

    @BeforeEach
    void initEach() {
        Queue accounts = accountService.getAllAccounts();
        if (!accounts.isEmpty()) {
            accounts.clear();
        }
    }

    /* getAccountBalance */

    @Test
    void getAccountBalance_emptyAccountList() {
        assertThrows(AccountNotFoundException.class, () -> monetaryOpService.getAccountBalance(1));
    }

    @Test
    void getAccountBalance_notExistingAccountId() {
        Account account = accountService.addAccount("getAccountBalance", "notExistingAccountId");
        assertThrows(AccountNotFoundException.class, () -> monetaryOpService.getAccountBalance(account.getId() + 1));
    }

    @Test
    void getAccountBalance_zeroBalance() throws AccountNotFoundException {
        Account account = accountService.addAccount("getAccountBalance", "zeroBalance");
        BigDecimal balance = monetaryOpService.getAccountBalance(account.getId());
        assertEquals(BigDecimal.ZERO, balance);
    }

    @Test
    void getAccountBalance_positiveBalance() throws BaseAppException {
        Account account = accountService.addAccount("getAccountBalance", "positiveBalance");
        monetaryOpService.addMoney(account.getId(), POS_AMOUNT);
        BigDecimal balance = monetaryOpService.getAccountBalance(account.getId());
        assertEquals(POS_AMOUNT, balance);
    }

    /* addMoney */

    @Test
    void addMoney_notExistingAccountId() {
        Account account = accountService.addAccount("addMoney", "notExistingAccountId");
        assertThrows(AccountNotFoundException.class,
                () -> monetaryOpService.addMoney(account.getId() + 1, POS_AMOUNT));
    }

    @Test
    void addMoney_amountNull() {
        Account account = accountService.addAccount("addMoney", "amountNull");
        assertThrows(InvalidInputException.class, () -> monetaryOpService.addMoney(account.getId(), null));
    }

    @Test
    void addMoney_amountZero() {
        Account account = accountService.addAccount("addMoney", "amountZero");
        assertThrows(InvalidInputException.class,
                () -> monetaryOpService.addMoney(account.getId(), BigDecimal.ZERO));
    }

    @Test
    void addMoney_amountNegative() {
        Account account = accountService.addAccount("addMoney", "amountNegative");
        assertThrows(InvalidInputException.class, () -> monetaryOpService.addMoney(account.getId(), NEG_AMOUNT));
    }

    @Test
    void addMoney_amountPositive() throws BaseAppException {
        Account account = accountService.addAccount("addMoney", "amountPositive");
        monetaryOpService.addMoney(account.getId(), POS_AMOUNT);
        assertEquals(POS_AMOUNT, account.getBalance());
        monetaryOpService.addMoney(account.getId(), BigDecimal.TEN);
        assertEquals(POS_AMOUNT.add(BigDecimal.TEN), account.getBalance());
    }

    /* withdrawMoney */

    @Test
    void withdrawMoney_notExistingAccountId() {
        Account account = accountService.addAccount("withdrawMoney", "notExistingAccountId");
        assertThrows(AccountNotFoundException.class,
                () -> monetaryOpService.withdrawMoney(account.getId() + 1, POS_AMOUNT));
    }

    @Test
    void withdrawMoney_amountNull() {
        Account account = accountService.addAccount("withdrawMoney", "amountNull");
        assertThrows(InvalidInputException.class,
                () -> monetaryOpService.withdrawMoney(account.getId(), null));
    }

    @Test
    void withdrawMoney_amountZero() {
        Account account = accountService.addAccount("withdrawMoney", "amountZero");
        assertThrows(InvalidInputException.class,
                () -> monetaryOpService.withdrawMoney(account.getId(), BigDecimal.ZERO));
    }

    @Test
    void withdrawMoney_amountNegative() {
        Account account = accountService.addAccount("withdrawMoney", "amountNegative");
        assertThrows(InvalidInputException.class,
                () -> monetaryOpService.withdrawMoney(account.getId(), NEG_AMOUNT));
    }

    @Test
    void withdrawMoney_success() throws BaseAppException {
        Account account = accountService.addAccount("withdrawMoney", "success");
        monetaryOpService.addMoney(account.getId(), POS_AMOUNT);
        monetaryOpService.withdrawMoney(account.getId(), BigDecimal.TEN);
        assertEquals(POS_AMOUNT.subtract(BigDecimal.TEN), account.getBalance());
        monetaryOpService.withdrawMoney(account.getId(), BigDecimal.ONE);
        assertEquals(POS_AMOUNT.subtract(BigDecimal.TEN).subtract(BigDecimal.ONE), account.getBalance());
    }

    @Test
    void withdrawMoney_notEnoughFundsBalanceZero() {
        Account account = accountService.addAccount("withdrawMoney", "notEnoughFundsBalanceZero");
        assertThrows(NotEnoughFundsException.class,
                () -> monetaryOpService.withdrawMoney(account.getId(), BigDecimal.ONE));
        assertEquals(BigDecimal.ZERO, account.getBalance());
    }

    @Test
    void withdrawMoney_notEnoughFundsBalancePositive() throws BaseAppException {
        Account account = accountService.addAccount("withdrawMoney", "notEnoughFundsBalancePositive");
        monetaryOpService.addMoney(account.getId(), BigDecimal.TEN);
        assertThrows(NotEnoughFundsException.class,
                () -> monetaryOpService.withdrawMoney(account.getId(), POS_AMOUNT));
        assertEquals(BigDecimal.TEN, account.getBalance());
    }

    /* transferMoney */

    @Test
    void transferMoney_notExistingAccountId() {
        Account account = accountService.addAccount("transferMoney", "notExistingAccountId");
        assertThrows(AccountNotFoundException.class,
                () -> monetaryOpService.transferMoney(account.getId(), account.getId() + 1, POS_AMOUNT));
        assertThrows(AccountNotFoundException.class,
                () -> monetaryOpService.transferMoney(account.getId() + 1, account.getId(), POS_AMOUNT));
    }

    @Test
    void transferMoney_amountNull() {
        Account senderAccount = accountService.addAccount("transferMoney", "amountNull_sender");
        Account receiverAccount = accountService.addAccount("transferMoney", "amountNull_receiver");
        assertThrows(InvalidInputException.class,
                () -> monetaryOpService.transferMoney(senderAccount.getId(), receiverAccount.getId(), null));
    }

    @Test
    void transferMoney_amountZero() {
        Account senderAccount = accountService.addAccount("transferMoney", "amountZero_sender");
        Account receiverAccount = accountService.addAccount("transferMoney", "amountZero_receiver");
        assertThrows(InvalidInputException.class,
                () -> monetaryOpService.transferMoney(senderAccount.getId(), receiverAccount.getId(), BigDecimal.ZERO));
    }

    @Test
    void transferMoney_amountNegative() {
        Account senderAccount = accountService.addAccount("transferMoney", "amountNegative_sender");
        Account receiverAccount = accountService.addAccount("transferMoney", "amountNegative_receiver");
        assertThrows(InvalidInputException.class,
                () -> monetaryOpService.transferMoney(senderAccount.getId(), receiverAccount.getId(), NEG_AMOUNT));
    }

    @Test
    void transferMoney_success() throws BaseAppException {
        Account account1 = accountService.addAccount("transferMoney", "success_account1");
        Account account2 = accountService.addAccount("transferMoney", "success_account2");

        monetaryOpService.addMoney(account1.getId(), POS_AMOUNT);
        monetaryOpService.transferMoney(account1.getId(), account2.getId(), BigDecimal.TEN);
        assertEquals(POS_AMOUNT.subtract(BigDecimal.TEN), account1.getBalance());
        assertEquals(BigDecimal.TEN, account2.getBalance());

        monetaryOpService.transferMoney(account2.getId(), account1.getId(), BigDecimal.TEN);
        assertEquals(POS_AMOUNT, account1.getBalance());
        assertEquals(BigDecimal.ZERO, account2.getBalance());
    }

    @Test
    void transferMoney_notEnoughFundsBalanceZero() {
        Account sender = accountService.addAccount("transferMoney", "notEnoughFundsBalanceZero_sender");
        Account receiver = accountService.addAccount("transferMoney", "notEnoughFundsBalanceZero_receiver");

        assertThrows(NotEnoughFundsException.class,
                () -> monetaryOpService.transferMoney(sender.getId(), receiver.getId(), BigDecimal.ONE));
        assertEquals(BigDecimal.ZERO, sender.getBalance());
        assertEquals(BigDecimal.ZERO, receiver.getBalance());
    }

    @Test
    void transferMoney_notEnoughFundsBalancePositive() throws BaseAppException {
        Account sender = accountService.addAccount("transferMoney", "notEnoughFundsBalancePositive_sender");
        Account receiver = accountService.addAccount("transferMoney", "notEnoughFundsBalancePositive_receiver");

        monetaryOpService.addMoney(sender.getId(), BigDecimal.TEN);
        assertThrows(NotEnoughFundsException.class,
                () -> monetaryOpService.transferMoney(sender.getId(), receiver.getId(), POS_AMOUNT));
        assertEquals(BigDecimal.TEN, sender.getBalance());
        assertEquals(BigDecimal.ZERO, receiver.getBalance());
    }
}
