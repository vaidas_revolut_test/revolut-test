package com.revolut.test.service;

import com.revolut.test.data.model.Account;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Queue;

import static org.junit.jupiter.api.Assertions.*;

class AccountServiceTest {
    private AccountService service = new AccountService();

    @BeforeEach
    void initEach() {
        Queue accounts = service.getAllAccounts();
        if (!accounts.isEmpty()) {
            accounts.clear();
        }
    }

    /* getAllAccounts */

    @Test
    void getAllAccounts_noAccounts() {
        Queue<Account> accounts = service.getAllAccounts();
        assertTrue(accounts.isEmpty());
    }

    @Test
    void getAllAccounts_oneAccount() {
        service.addAccount("getAllAccounts", "oneAccount");
        Queue<Account> accounts = service.getAllAccounts();
        assertEquals(1, accounts.size());
    }

    @Test
    void getAllAccounts_multipleAccounts() {
        service.addAccount("getAllAccounts1", "multipleAccounts1");
        service.addAccount("getAllAccounts2", "multipleAccounts2");
        service.addAccount("getAllAccounts3", "multipleAccounts3");
        Queue<Account> accounts = service.getAllAccounts();
        assertEquals(3, accounts.size());
    }

    /* findAccount */

    @Test
    void findAccount_noAccounts() {
        assertNull(service.findAccount(1));
    }

    @Test
    void findAccount_notExistingAccount() {
        Account account = service.addAccount("findAccount", "notExistingAccount");
        assertNull(service.findAccount(account.getId() + 1));
    }

    @Test
    void findAccount_existingAccount() {
        Account createdAccount = service.addAccount("findAccount", "existingAccount");
        Account foundAccount = service.findAccount(createdAccount.getId());
        assertNotNull(foundAccount);
        assertEquals(createdAccount, foundAccount);
    }

    /* addAccount */

    @Test
    void addAccount_nullOrEmpty() {
        assertThrows(IllegalArgumentException.class, () -> service.addAccount(null, "nullOrEmpty"));
        assertThrows(IllegalArgumentException.class, () -> service.addAccount("addAccount", null));
        assertThrows(IllegalArgumentException.class, () -> service.addAccount("", "nullOrEmpty"));
        assertThrows(IllegalArgumentException.class, () -> service.addAccount("addAccount", ""));
        assertTrue(service.getAllAccounts().isEmpty());
    }

    @Test
    void addAccount_success() {
        Account createdAccount = service.addAccount("addAccount", "success");
        assertEquals("addAccount", createdAccount.getFirstName());
        assertEquals("success", createdAccount.getLastName());
        assertEquals(1, service.getAllAccounts().size());
    }

    /* removeAccount */

    @Test
    void removeAccount_emptyAccountList() {
        assertFalse(service.removeAccount(1));
        assertFalse(service.removeAccount(0));
        assertFalse(service.removeAccount(-1));
    }

    @Test
    void removeAccount_notExistingAccountId() {
        Account createdAccount = service.addAccount("removeAccount", "notExistingAccountId");
        assertFalse(service.removeAccount(createdAccount.getId() + 1));
        Account foundAccount = service.findAccount(createdAccount.getId());
        assertNotNull(foundAccount);
        assertEquals(createdAccount, foundAccount);
    }

    @Test
    void removeAccount_existingAccountId() {
        Account createdAccount = service.addAccount("removeAccount", "existingAccountId");
        assertTrue(service.removeAccount(createdAccount.getId()));
        Account foundAccount = service.findAccount(createdAccount.getId());
        assertNull(foundAccount);
    }
}
